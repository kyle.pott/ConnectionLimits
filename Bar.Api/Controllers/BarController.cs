﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;

namespace Bar.Api.Controllers
{
    public class BarController : ApiController
    {
        public Task<string> Get()
        {
            const int interval = 5000;

            var source = new TaskCompletionSource<string>();
            new Timer(_ => source.SetResult(string.Format("This is bar! {0}", DateTime.UtcNow))).Change(interval, -1);
            return source.Task;
        }
    }
}