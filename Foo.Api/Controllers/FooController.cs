﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Http;

namespace Foo.Api.Controllers
{
    public class FooController : ApiController
    {
        public string Get()
        {
            return MakeWebRequest();
        }

        private string MakeWebRequest()
        {
            var barServiceBaseUrl = ConfigurationManager.AppSettings["BarServiceBaseUrl"];

            var uri = new Uri(barServiceBaseUrl);
            var sp = ServicePointManager.FindServicePoint(uri);
            Trace.WriteLine(string.Format("ServicePoint ConnectionName={0} ConnectionLimit={1} CurrentConnections={2}", sp.ConnectionName, sp.ConnectionLimit, sp.CurrentConnections));

            var request = (HttpWebRequest)WebRequest.Create(string.Format("{0}/api/bar", barServiceBaseUrl));

            var response = (HttpWebResponse)request.GetResponse();
            var receiveStream = response.GetResponseStream();
            var readStream = new StreamReader(receiveStream, Encoding.UTF8);

            var result = readStream.ReadToEnd();

            response.Close();
            readStream.Close();

            return result;
        }
    }
}